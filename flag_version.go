package mkh_utils

import (
	"flag"
	"log"
	"os"
	"strconv"
	"time"
)

var (
	Version     string
	Branch      string
	Commit      string
	BuildTime   string
	CompanyLogo string
	// ServerType 服务类型
	ServerType string
	// AppEnv 运行环境
	AppEnv string
	// Expiration 到期时间
	Expiration time.Time
	// ExpirationMonth 过期月数
	ExpirationMonth string
)

func CheckVersion() {
	versionFlag := flag.Bool("version", false, "print the version")
	flag.Parse()
	if ExpirationMonth != "" {
		expirationMonth, err := strconv.Atoi(ExpirationMonth)
		if err != nil {
			log.Printf("时间转换失败:%s\n", BuildTime)
			return
		}
		layout := "2006-01-02 15:04:05" // 指定输入字符串的日期和时间格式
		t, err := time.Parse(layout, BuildTime)
		if err != nil {
			log.Printf("时间转换失败:%s\n", BuildTime)
		}
		Expiration = t.AddDate(0, expirationMonth, 0)
	}
	if *versionFlag {
		log.Printf("Version: %s\n", Version)
		log.Printf("Branch: %s\n", Branch)
		log.Printf("Commit: %s\n", Commit)
		log.Printf("BuildTime: %s\n", BuildTime)
		log.Printf("CompanyLogo: %s\n", CompanyLogo)
		log.Printf("ServerType: %s\n", ServerType)
		log.Printf("AppEnv: %s\n", AppEnv)
		log.Printf("Expiration: %v\n", Expiration)
		os.Exit(0)
	}
}
